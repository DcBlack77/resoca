<?php

namespace App\Modules\Ventasbrink\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'resoca';

	protected $patch_js = [
		'public/js',
		'public/plugins',
		'app/Modules/Ventasbrink/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'app/Modules/Ventasbrink/Assets/css',
	];
}
